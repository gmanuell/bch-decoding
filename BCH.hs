-- an implementation of algorithms for encoding and decoding a 256-ary [255,245,11] BCH code

import FiniteFields
import Polynomials

import Data.List
import Data.Char
import Control.Applicative
import Control.Monad
import Control.Monad.Writer
import System.Environment
import System.IO
import System.Random

type GF256 = PolynomialField F256Poly Z2

-- the length of the codewords in the code
codelength = 255
-- the primitive root used in the BCH code
primitiveRoot :: GF256
primitiveRoot = primitiveElement
-- the defining set of the BCH code with respect to the above primitive root
definingset = [0..9]
-- b as defined in the definition of BCH codes
b = 0
-- the designed distance of the BCH code
delta = 11

-- modulus used in by the cylcic code
modulus :: Polynomial GF256
modulus = Polynomial $ [-1] ++ replicate (codelength-1) 0 ++ [1]

-- generating function used by the BCH code
g :: Polynomial GF256
g = product $ map (\i -> Polynomial [-primitiveRoot^i,1]) definingset

errorsCorrectable = (delta-1) `div` 2

encode :: Polynomial GF256 -> Polynomial GF256
encode = normalise . (g*)

-- construct the syndrome polynomial for an error
syndromePoly error = Polynomial . take (delta-1) $ syndromes
  where syndromes = map (eval error) . iterate (*primitiveRoot) $ (primitiveRoot^b)

euclidDecode syndromes = egcd' syndromes (Polynomial $ replicate (delta-1) 0 ++ [1]) 0 1 1 0 where
  egcd' r rprev u v uprev vprev =
    if degree r < (delta-1) `div` 2
      then (v, r)
      else let  (q, rnext) = divModPoly rprev r
                unext  = uprev - q*u
                vnext  = vprev - q*v
        in egcd' rnext r unext vnext u v

-- find the roots of a polynomial as the powers of primitiveRoot
chienSearch (Polynomial xs) =
  let  evalOnPowers   = map sum . iterate (zipWith (*) $ iterate (*primitiveRoot) 1) $ xs
       lookupAllZero  = map fst . filter ((==0).snd)
  in lookupAllZero $ zip [0..codelength-1] evalOnPowers

-- construct the error polynomial from the error locations and the errors at those locations 
errorPoly :: [Int] -> [GF256] -> Polynomial GF256
errorPoly locations errors = Polynomial [maybe 0 id . lookup i $ zip locations errors | i <- [0..codelength-1]]

-- the Forney algorithm: finds the error at the location represented by xj
forney :: Polynomial GF256 -> Polynomial GF256 -> GF256 -> Maybe GF256
forney lambda omega xj = case eval (differentiate lambda) $ recip xj of
  0      -> Nothing
  denom  -> Just $ (-xj/xj^b) * (eval omega $ recip xj) / denom

-- decode a received polynomial and record information about the decoding with a Writer monad
decode :: Polynomial GF256 -> Writer String (Maybe (Polynomial GF256))
decode received = do
  let (lambda, omega) = euclidDecode . syndromePoly $ received
  let locations = map ((`mod` codelength) . negate) $ chienSearch lambda
  if length locations > errorsCorrectable
    then do
      tell "Too many errors; could not decode\n"
      return Nothing
    else do
      let errors = mapM (forney lambda omega) $ map (primitiveRoot^) locations
      let corrected = received - maybe 0 (errorPoly locations) errors
      let (decoded, zero) = corrected `divModPoly` g
      if zero /= 0
        then do
          tell "Too many errors; could not decode\n"
          return Nothing
        else do
          tell . (++) (show (length locations) ++ " error(s) found:\n") . unlines $
            zipWith (\i e -> show i ++ ": " ++ (show.fieldElementToBinary) e) locations $ maybe [] id errors
          return . Just . normalise $ decoded

-- map an integer to an element of GF256 based on its binary expansion
binaryToFieldElement :: Integer -> GF256
binaryToFieldElement i = fromPolynomial . Polynomial $ map fromInteger (map (i `div`) $ map (2^) [0..7])

-- the inverse operation to binaryToFieldElement mapping integers to field elements
fieldElementToBinary :: GF256 -> Integer
fieldElementToBinary = flip eval 2 . rawPolynomialValue

-- represent a string as a polynomial over GF256 using ASCII encoding 
stringToPoly :: String -> Polynomial GF256
stringToPoly s = Polynomial . map (binaryToFieldElement . toInteger . ord) $ s

-- decode the string from the polynomial
polyToString :: Polynomial GF256 -> String
polyToString (Polynomial xs) = chr . fromInteger . fieldElementToBinary <$> xs

main = do
  args <- getArgs
  let arg = if null args then "" else head args
  let verbose = "-v" `elem` args
  case arg of
    "encode" -> showPoly . encode . stringToPoly <$> getLine >>= putStrLn
    "decode" -> do
      (result, notes) <- runWriter . decode . readPoly <$> getLine
      when verbose $ hPutStr stderr notes
      putStrLn $ maybe "???" polyToString result
    "error" -> do
      error <- randomError verbose -- simulate errors
      showPoly . (+ error) . readPoly <$> getLine >>= putStrLn
    _ -> getProgName >>= (\prog -> putStrLn $ "Usage: " ++ prog ++ " (encode|decode|error) [-v]" )
  where
    showFieldElement = show . fieldElementToBinary
    showPoly = unwords . map showFieldElement . getCoefficients
    readPoly = Polynomial . map (binaryToFieldElement . read) . words
    randomError verbose = do
      (num, gen) <- randomR (0,errorsCorrectable+1) <$> newStdGen
      let locations = take num . randomRs (0, codelength-1) $ gen :: [Int]
      errors <- map binaryToFieldElement . take num . randomRs (1,255) <$> newStdGen
      when verbose . hPutStr stderr . (++) ((show num) ++ " error(s) added:\n") . unlines $
        zipWith (\i e -> show i ++ ": " ++ showFieldElement e) locations errors
      return . normalise $ errorPoly locations errors


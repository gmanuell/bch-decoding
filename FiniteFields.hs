{-# LANGUAGE ScopedTypeVariables, EmptyDataDecls, MultiParamTypeClasses, FunctionalDependencies #-}
-- data structures and functions for handling finite fields

module FiniteFields (FiniteField(..), PrimeField, PrimeType, Z2, PolynomialField, fromSubfield,
  fromPolynomial, IrreduciblePolyType, F256Poly, rawIntegerValue, rawPolynomialValue) where

import Polynomials

import Ratio (denominator, numerator)
import Control.Applicative

class (Fractional alpha) => FiniteField alpha where
  -- this is a function so the finite field in question can be determined
  characteristic :: alpha -> Integer
  fieldSize :: alpha -> Integer
  primitiveElement :: alpha

-- a typeclass for types representing the sizes of prime fields (to simulate dependent types)
class PrimeType pi where
  typeAsValue :: pi -> Integer -- the value
  primitiveRootModValue :: pi -> Integer -- a generator of the multiplicative group of the field of that size

data Z2 -- a type representing the value 2
instance PrimeType Z2 where
  typeAsValue = const 2
  primitiveRootModValue = const 1

newtype (PrimeType pi) => PrimeField pi = Fp Integer deriving (Eq)

rawIntegerValue (Fp n) = n

instance (PrimeType pi) => Show (PrimeField pi) where
  show (Fp v) = show v

instance (PrimeType pi) => Num (PrimeField pi) where
  (Fp x) + (Fp y) = fromInteger $ x+y
  (Fp x) * (Fp y) = fromInteger $ x*y
  fromInteger = Fp . (`mod` characteristic (undefined::PrimeField pi))
  negate = (* fromInteger (-1))
  abs = undefined
  signum = undefined

instance (PrimeType pi) => Fractional (PrimeField pi) where
  recip (Fp 0) = undefined
  recip (Fp x) = Fp . snd . egcd (characteristic (undefined::PrimeField pi)) $ x
  fromRational = liftA2 (/) (fromInteger . numerator) (fromInteger . denominator)

egcd :: Integral eta => eta -> eta -> (eta, eta)
egcd a b = egcd' b a 0 1 1 0 where
  egcd' 0 rprev u v uprev vprev = (uprev, vprev)
  egcd' r rprev u v uprev vprev = 
    let (q, rnext) = divMod rprev r
        unext  = uprev - q*u
        vnext  = vprev - q*v
    in egcd' rnext r unext vnext u v

instance (PrimeType pi) => FiniteField (PrimeField pi) where
  characteristic = const $ typeAsValue (undefined::pi)
  fieldSize = characteristic
  primitiveElement = fromInteger $ primitiveRootModValue (undefined::pi)

-- a typeclass for types representing the ideals used to construct finite fields from polynomial rings
class (PrimeType pi) => IrreduciblePolyType sigma pi | sigma -> pi where
  typeAsPoly :: sigma -> Polynomial (PrimeField pi) -- the irreducible polynomial
  generatorPolynomial :: sigma -> Polynomial (PrimeField pi) -- a generator of the resulting field

data F256Poly
instance IrreduciblePolyType F256Poly Z2 where
  typeAsPoly = const $ Polynomial [1,0,1,1,1,0,0,0,1]
  generatorPolynomial = const $ Polynomial [0,1]

newtype (IrreduciblePolyType sigma pi) => PolynomialField sigma pi = Fq (Polynomial (PrimeField pi)) deriving (Eq)

rawPolynomialValue (Fq (Polynomial phi)) = Polynomial . map rawIntegerValue $ phi

fromSubfield :: (IrreduciblePolyType sigma pi) => PrimeField pi -> PolynomialField sigma pi
fromSubfield = Fq . Polynomial . return

fromPolynomial :: forall sigma pi. (IrreduciblePolyType sigma pi) => Polynomial (PrimeField pi) -> PolynomialField sigma pi
fromPolynomial = Fq . normalise . (`modPoly` typeAsPoly (undefined::sigma))

instance (IrreduciblePolyType sigma pi) => Show (PolynomialField sigma pi) where
  show (Fq v) = "{" ++ (map (\x -> if x == 'x' then 't' else x) $ show v) ++ "}"

instance (IrreduciblePolyType sigma pi) => Num (PolynomialField sigma pi) where
  (Fq x) + (Fq y) = fromPolynomial $ x+y
  (Fq x) * (Fq y) = fromPolynomial $ x*y
  fromInteger = fromSubfield . fromInteger
  negate = (* fromInteger (-1))
  abs = undefined
  signum = undefined

instance (IrreduciblePolyType sigma pi) => Fractional (PolynomialField sigma pi) where
  recip (Fq 0) = undefined
  recip (Fq b) =
    -- the gcd of coprime polynomials is an invertible constant c
    let (x,y,gcd) = egcdPoly (typeAsPoly (undefined::sigma)) $ b
        c         = head $ getCoefficients gcd
    in fromPolynomial $ (recip c).*y -- divide by c to find the modular inverse
  fromRational = fromSubfield . fromRational

instance (IrreduciblePolyType sigma pi) => FiniteField (PolynomialField sigma pi) where
  characteristic = const $ typeAsValue (undefined::pi)
  fieldSize dummy = characteristic dummy ^ (degree $ typeAsPoly (undefined::sigma))
  primitiveElement = fromPolynomial $ generatorPolynomial (undefined::sigma)


-- data structures and functions for manipulating polynomials

module Polynomials (Polynomial(..), normalise, degree, eval, (.*), divModPoly, divPoly, modPoly,
                    egcdPoly, differentiate) where

import Data.List
import Control.Monad
import Control.Arrow ((***))
import Data.Function (on)

-- a polynomial is represented as a list of coefficents in order of increasing powers of x
newtype Polynomial alpha = Polynomial { getCoefficients :: [alpha] }

-- find a unqiue polynomial representation by truncating trailing zeros
normalise :: (Num alpha) => Polynomial alpha -> Polynomial alpha
normalise (Polynomial (x:xs)) = Polynomial $ x : ( reverse . dropWhile (==0) . reverse $ xs )
normalise (Polynomial []) = Polynomial [0]

degree :: (Num alpha) => Polynomial alpha -> Int
degree = (subtract 1) . length . getCoefficients . normalise

eval :: (Num alpha) => Polynomial alpha -> alpha -> alpha
eval (Polynomial cs) x = foldr (\c a -> a*x+c) 0 cs

-- multiply by a scalar
infixl 7 .*
s .* (Polynomial cs) = Polynomial $ map (s*) cs

instance (Num alpha) => Eq (Polynomial alpha) where
  (Polynomial (x:xs)) == (Polynomial (y:ys)) = x==y && (Polynomial xs) == (Polynomial ys)
  _ == (Polynomial ys@(_:_)) = all (==0) ys
  (Polynomial xs) == _ = all (==0) xs

instance (Show alpha) => Show (Polynomial alpha) where
  show (Polynomial cs) = let xs = "":"x": ["x^" ++ (show n) | n <- [2..]]
    in intercalate "+" $ zipWith (++) (map show cs) xs

instance (Num alpha) => Num (Polynomial alpha) where
  (Polynomial x) + (Polynomial y) = Polynomial $ add x y where
    add (x:xs) (y:ys) = x+y : add xs ys
    add [] ys = ys
    add xs [] = xs
  
  (Polynomial (x:xs)) * (Polynomial (y:ys)) = Polynomial $ x*y : getCoefficients rest where
    rest = (x .* Polynomial ys) + (Polynomial xs)*(Polynomial $ y:ys)
  _ * _ = Polynomial []
  
  negate = Polynomial . map negate . getCoefficients
  fromInteger n = Polynomial [fromInteger n]
  abs = undefined
  signum = undefined

infixl 7 `divModPoly`
divModPoly :: Fractional alpha => Polynomial alpha -> Polynomial alpha -> (Polynomial alpha,  Polynomial alpha)
divModPoly (Polynomial xs) (Polynomial ys) =
  join (***) (Polynomial . reverse) $ ( divModReversed `on` dropWhile (==0) . reverse ) xs ys where
    divModReversed (x:xs) (y:ys)
      | length xs >= length ys =
        let (current, bringDown) = splitAt (length ys) xs
            q = x/y
            (d,m) = divModReversed (zipWith (-) current (map (*q) ys) ++ bringDown) (y:ys)
        in (q:d, m)
      | otherwise = ([],x:xs)
    divModReversed _ [] = (undefined, undefined)
    divModReversed _ _ = ([], [])

infixl 7 `divPoly`
divPoly :: Fractional alpha => Polynomial alpha -> Polynomial alpha -> Polynomial alpha
divPoly p q = fst $ divModPoly p q

infixl 7 `modPoly`
modPoly :: Fractional alpha => Polynomial alpha -> Polynomial alpha -> Polynomial alpha
modPoly p q = snd $ divModPoly p q

-- the Extended Euclidean algorithm: given (a,b), it returns (x,y,gcd(a,b)) such that ax+by = gcd(a,b)
egcdPoly :: Fractional alpha => Polynomial alpha -> Polynomial alpha -> (Polynomial alpha, Polynomial alpha, Polynomial alpha)
egcdPoly a b = egcd' b a 0 1 1 0 where
  egcd' 0 rprev u v uprev vprev = (uprev, vprev, rprev)
  egcd' r rprev u v uprev vprev =
    let (q, rnext) = divModPoly rprev r
        unext  = uprev - q*u
        vnext  = vprev - q*v
    in egcd' rnext r unext vnext u v

-- calculate the formal derivative of a polynomial
differentiate :: Num alpha => Polynomial alpha -> Polynomial alpha
differentiate = Polynomial . drop 1 . zipWith ((*) . fromInteger) [0..] . getCoefficients

